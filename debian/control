Source: openstack-doc-tools
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Corey Bryant <corey.bryant@canonical.com>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper (>= 10),
 dh-python,
 openstack-pkg-tools,
 python-all,
 python-pbr (>= 2.0.0),
 python-setuptools,
 python3-all,
 python3-pbr (>= 2.0.0),
 python3-setuptools,
 python3-sphinx (>= 1.6.2),
Build-Depends-Indep:
 libxml2-dev,
 libxslt-dev,
 pylint,
 python-bashate (>= 0.5.1),
 python-demjson,
 python-doc8,
 python-docutils,
 python-hacking (>= 0.10.0),
 python-iso8601,
 python-lxml,
 python-yaml,
 python3-openstackdocstheme (>= 1.18.1),
Standards-Version: 4.1.3
Homepage: https://github.com/openstack/openstack-doc-tools
Vcs-Git: https://salsa.debian.org/openstack-team/libs/openstack-doc-tools.git
Vcs-Browser: https://salsa.debian.org/openstack-team/libs/openstack-doc-tools

Package: openstack-doc-tools-common
Architecture: all
Depends:
 ${misc:Depends},
 ${python:Depends},
 ${sphinxdoc:Depends},
Description: tools used by the OpenStack Documentation project - common files
 This package contains a set of tools used to build the OpenStack documentation
 (install-guide, etc.) in multiple formats: html, pdf, etc. The input format is
 a set of XML documents.
 .
 This package contains common files for both the Python 2.7 and 3.x packages.

Package: python-openstack-doc-tools
Architecture: all
Depends:
 libjs-bootstrap,
 libjs-jquery,
 libxml2-dev,
 libxslt-dev,
 openstack-doc-tools-common (= ${binary:Version}),
 python-demjson,
 python-docutils,
 python-iso8601,
 python-lxml,
 python-pbr (>= 2.0.0),
 python-sphinx,
 python-yaml,
 ${misc:Depends},
 ${python:Depends},
 ${sphinxdoc:Depends},
Description: tools used by the OpenStack Documentation project - Python 2.7
 This package contains a set of tools used to build the OpenStack documentation
 (install-guide, etc.) in multiple formats: html, pdf, etc. The input format is
 a set of XML documents.
 .
 This package provides the Python 2.7 module.

Package: python3-openstack-doc-tools
Architecture: all
Depends:
 libxml2-dev,
 libxslt-dev,
 openstack-doc-tools-common (= ${binary:Version}),
 python3-demjson,
 python3-docutils,
 python3-iso8601,
 python3-lxml,
 python3-pbr (>= 2.0.0),
 python3-sphinx,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
 ${sphinxdoc:Depends},
Description: tools used by the OpenStack Documentation project - Python 3.x
 This package contains a set of tools used to build the OpenStack documentation
 (install-guide, etc.) in multiple formats: html, pdf, etc. The input format is
 a set of XML documents.
 .
 This package provides the Python 3.x module.
