#!/usr/bin/make -f

include /usr/share/openstack-pkg-tools/pkgos.make

export LAST_CHANGE=$(shell dpkg-parsechangelog -S Date)
export BUILD_DATE=$(shell LC_ALL=C date -u "+%B %d, %Y" -d "$(LAST_CHANGE)")

%:
	dh $@ --buildsystem=python_distutils --with python2,python3,sphinxdoc

override_dh_clean:
	dh_clean -O--buildsystem=python_distutils
	rm -rf doc/build
	rm -rf build
	rm -rf openstack_doc_tools.egg-info
	rm -f AUTHORS ChangeLog

override_dh_auto_install:
	pkgos-dh_auto_install

	# Move the cleanup and sitemap to the openstack-doc-tools-common package
	mkdir -p $(CURDIR)/debian/openstack-doc-tools-common/usr/share/openstack-doc-tools
	mv $(CURDIR)/debian/python-openstack-doc-tools/usr/share/openstack-doc-tools/sitemap $(CURDIR)/debian/openstack-doc-tools-common/usr/share/openstack-doc-tools
	rm -rf $(CURDIR)/debian/python*-openstack-doc-tools/usr/share/openstack-doc-tools/*
	find $(CURDIR)/debian/python3-openstack-doc-tools/usr/share/openstack-doc-tools -exec sed -i '1 s/python/python3/' {} \;

override_dh_python2:
	dh_python2 -ppython-openstack-doc-tools
	dh_python2 -ppython-openstack-doc-tools /usr/share

override_dh_python3:
	dh_python3 -ppython3-openstack-doc-tools --shebang=/usr/bin/python3
	dh_python3 -ppython3-openstack-doc-tools --shebang=/usr/bin/python3 /usr/share

override_dh_sphinxdoc:
ifeq (,$(findstring nodocs, $(DEB_BUILD_OPTIONS)))
	python3 -m sphinx -b html doc/source $(CURDIR)/debian/python-openstack-doc-tools/usr/share/doc/python-openstack-doc-tools/html
	dh_sphinxdoc -O--buildsystem=python_distutils
endif

override_dh_installman:
ifeq (,$(findstring nodocs, $(DEB_BUILD_OPTIONS)))
	python3 -m sphinx -b man doc/source doc/build/man
	dh_installman -O--buildsystem=python_distutils
endif
